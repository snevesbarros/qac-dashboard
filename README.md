# qac-dashboard
A test dashboard that will allow you to store your automated test results in a common way an display the reports from various test execution frameworks

### create docker volumes
docker volume create --name=qac_mongo_config
docker volume create --name=qac_mongo_db
